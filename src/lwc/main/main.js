/**
 * Created by oleg_zykyi on 11/15/19.
 */

import { LightningElement, track } from 'lwc';
import getVMAddresses from '@salesforce/apex/VisionCareService.getVMAddresses';
import getAllVMAddresses from '@salesforce/apex/VisionCareService.getAllVMAddresses';

export default class Main extends LightningElement {

@track markers;
@track error;

connectedCallback() {
    this.loadVMAddresses();
}

loadVMAddresses() {
    getAllVMAddresses()
    .then(result => {
        this.markers = result;
    })
    .catch(error => {
        this.error = error;
    })
}

handleAssetSelected(evt) {
    getVMAddresses({productId : evt.detail.Product2Id})
    .then(result => {
        this.markers = result;
    })
    .catch(error => {
        this.error = error;
    })
}

}