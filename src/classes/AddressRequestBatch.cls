/**
 * Created by Dmitry on 20.11.2019.
 */

public with sharing class AddressRequestBatch implements Database.Batchable<SObject>, Database.AllowsCallouts {
    List<Vending_Machine__c> machines;
    static String ENDPOINT = 'https://reverse.geocoder.api.here.com/6.2/reversegeocode.json?app_id=P2ERdYEIxNPENEu2tfEh&' +
            'app_code=gsKo-Z8mCfVqb_dSBohd9A&mode=retrieveAddresses&language=en&locationattributes=none,address&prox=';

    static String GET_METHOD = 'GET';

    static String RESPONSE_KEY = 'Response';
    static String VIEW_Key = 'View';
    static String RESULT_KEY = 'Result';
    static String LOCATION_KEY = 'Location';
    static String ADDRESS_KEY = 'Address';
    static String LABEL_KEY = 'Label';

    public AddressRequestBatch(List<Vending_Machine__c> machines) {
        this.machines = machines;
    }

    public Iterable<sObject> start(Database.BatchableContext bc) {
        return machines;
    }

    public void execute(Database.BatchableContext bc, List<Vending_Machine__c> records) {
        List<Vending_Machine__c> updatedMachines = new List<Vending_Machine__c>();
        for (Vending_Machine__c machine : records) {
            if(machine.Location__Longitude__s == null || machine.Location__Latitude__s == null){
                continue;
            }
            HttpResponse response = makeCallout(machine);
            String address = parseAddress(response.getBody());
            if (address != null) {
                machine.Address__c = address;
                updatedMachines.add(machine);
            }
        }
        if (!updatedMachines.isEmpty()) {
            update updatedMachines;
        }
    }

    HttpResponse makeCallout(Vending_Machine__c machine) {
        HttpRequest request = new HttpRequest();
        request.setMethod(GET_METHOD);
        request.setEndpoint(getEndpoint(machine));
        Http http = new Http();
        return http.send(request);
    }

    String getEndpoint(Vending_Machine__c machine) {
        return ENDPOINT + machine.Location__Latitude__s + ',' + machine.Location__Longitude__s + ',1';
    }

    String parseAddress(String body) {
        Map<String, Object> params = (Map<String, Object>) JSON.deserializeUntyped(body);
        Map<String, Object> resp = (Map<String, Object>) params.get(RESPONSE_KEY);
        if (resp == null) {
            return null;
        }
        List<Object> viewList = (List<Object>) resp.get(VIEW_Key);
        if (viewList == null || viewList.isEmpty()) {
            return null;
        }
        Map<String, Object> firstView = (Map<String, Object>) viewList.get(0);
        List<Object> resultList = (List<Object>) firstView.get(RESULT_KEY);
        if (resultList == null || resultList.isEmpty()) {
            return null;
        }
        Map<String, Object> result = (Map<String, Object>) resultList.get(0);
        Map<String, Object> location = (Map<String, Object>) result.get(LOCATION_KEY);
        if (location == null) {
            return null;
        }
        Map<String, Object> address = (Map<String, Object>) location.get(ADDRESS_KEY);
        if (address == null) {
            return null;
        }
        Object label = address.get(LABEL_KEY);
        return (label == null) ? null : label + '';
    }

    public void finish(Database.BatchableContext bc) {

    }
}