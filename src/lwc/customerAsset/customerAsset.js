import { LightningElement, api } from 'lwc';

export default class CustomerAsset extends LightningElement {

    @api asset;

    assetClick(){
        const assetClickEvent = new CustomEvent("assetclick", {
                detail : this.asset
            });
            this.dispatchEvent(assetClickEvent);
    }
}