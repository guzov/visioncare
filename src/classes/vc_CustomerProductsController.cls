/**
 * Created by oleg_zykyi on 11/14/19.
 */

public with sharing class vc_CustomerProductsController {

    public static List<Asset> getCustomerAssets(){
        return [
                SELECT Id,
                        Product2.Name,
                        PurchaseDate
                FROM Asset
        ];
    }

}