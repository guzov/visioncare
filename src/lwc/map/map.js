import { LightningElement, api } from 'lwc';

export default class LightningExampleMapSingleMarker extends LightningElement {

    @ api markers = [];
}