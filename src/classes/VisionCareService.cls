public with sharing class VisionCareService {

    @AuraEnabled
    public static List<Asset> getCustomerAssets() {
        return [SELECT Id, Product2.Name, Product2Id, PurchaseDate FROM Asset ORDER BY PurchaseDate];
    }

    @AuraEnabled
    public static List<LenseWrapper> getProductsWithPreferences() {
        List<Product2> allProducts = [
                SELECT Id, Name, Lense_Type__c, Lense_Replacement_period__c
                FROM Product2
                WHERE Family = 'Contact Lenses'
        ];
        List<Asset> assets = [
                SELECT Product2Id, PurchaseDate
                FROM Asset
                WHERE OwnerId = :UserInfo.getUserId()
                ORDER BY PurchaseDate
        ];
        Map<String, String> preferenceMap = getPreferenceMap();
        List<LenseWrapper> ratingList = new List<LenseWrapper>();
        for (Product2 item : allProducts) {
            ratingList.add(new LenseWrapper(item, assets, preferenceMap));
        }
        ratingList.sort();

        return ratingList;
    }

    static Map<String, String> getPreferenceMap() {
        User userWithPreferences = [
                select Contact.Lense_type__c, Contact.Lense_replacement_period__c
                from User
                where id = :UserInfo.getUserId()
        ];
        Map<String, String> preferenceMap = new Map<String, String>();
        if (userWithPreferences.Contact.Lense_type__c != null) {
            preferenceMap.put('Lense_Type__c', userWithPreferences.Contact.Lense_type__c);
        }
        if (userWithPreferences.Contact.Lense_replacement_period__c != null) {
            preferenceMap.put('Lense_Replacement_period__c', userWithPreferences.Contact.Lense_replacement_period__c);
        }
        return preferenceMap;
    }


    @AuraEnabled
    public static List<VMAddress> getVMAddresses(String productId) {
        List<VMAddress> vmAddresses = new List<VMAddress>();
        for (Vending_Machine_Product__c vmp : [
                SELECT Id, Vending_Machine__c, Vending_Machine__r.Location__Latitude__s,
                        Vending_Machine__r.Location__Longitude__s
                FROM Vending_Machine_Product__c
                WHERE Product__c = :productId
        ]) {
            Location location = new Location(vmp.Vending_Machine__r.Location__Latitude__s,
                    vmp.Vending_Machine__r.Location__Longitude__s);
            vmAddresses.add(new VMAddress(location));
        }
        return vmAddresses;
    }

    @AuraEnabled
    public static Contact getUserContact() {
        User userWithContact = [
                select Contact.Id, Contact.Lense_type__c, Contact.Lense_replacement_period__c
                from User
                where id = :UserInfo.getUserId()
        ];
        return userWithContact.Contact;
    }

    @AuraEnabled
    public static String generateCode(String productId) {
        String out = '';
        if (productId != null && productId != '') {
            User userWithContact = [SELECT Contact.Vision_Care_Consumer_Id__c FROM User WHERE Id = :UserInfo.getUserId()];
            Product2 productWithKey = [SELECT Vision_Care_Product_Id__c FROM Product2 WHERE Id = :productId];
            String userConsumerId = userWithContact.Contact.Vision_Care_Consumer_Id__c;
            String productConsumerId = productWithKey.Vision_Care_Product_Id__c;
            if (userConsumerId != null && productConsumerId != null) {
                out = Integer.valueOf(userConsumerId) + '-' + Integer.valueOf(productConsumerId);
            }
        }
        return out;
    }

    @AuraEnabled
    public static List<VMAddress> getAllVMAddresses() {
        List<VMAddress> vmAddresses = new List<VMAddress>();
        for (Vending_Machine__c vm : [SELECT Id, Location__Latitude__s, Location__Longitude__s FROM Vending_Machine__c]) {
            Location location = new Location(vm.Location__Latitude__s,
                    vm.Location__Longitude__s);
            vmAddresses.add(new VMAddress(location));
        }
        return vmAddresses;
    }


    public class VMAddress {
        @AuraEnabled
        public Location location;

        public VMAddress(Location location) {
            this.location = location;
        }
    }

    public class Location {
        @AuraEnabled
        public Decimal Latitude;
        @AuraEnabled
        public Decimal Longitude;

        public Location(Decimal latitude, Decimal longitude) {
            this.latitude = latitude;
            this.longitude = longitude;
        }
    }

    public class LenseWrapper implements Comparable {
        @AuraEnabled
        public Integer rating = 0;
        @AuraEnabled
        public Date lastPurchaseDate = null;
        @AuraEnabled
        public Product2 product;

        LenseWrapper(Product2 product, List<Asset> assets, Map<String, String> preferenceMap) {
            this.product = product;
            for (Asset asset : assets) {
                if (asset.Product2Id == product.Id) {
                    lastPurchaseDate = asset.PurchaseDate;
                    rating += (7 * (assets.size() + assets.indexOf(asset))) / assets.size();
                }
            }
            for (String fieldName : preferenceMap.keySet()) {
                rating += (product.get(fieldName) == preferenceMap.get(fieldName)) ? 5 : 0;
            }
        }

        public Integer compareTo(Object compareTo) {
            LenseWrapper comparing = (LenseWrapper) compareTo;
            return comparing.rating - this.rating;
        }
    }

}