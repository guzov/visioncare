import { LightningElement, track } from 'lwc';
import getCustomerAssets from '@salesforce/apex/VisionCareService.getCustomerAssets';

export default class CustomerAssets extends LightningElement {

@track customerAssets;
@track error;

connectedCallback() {
    this.loadCustomerAssets();
}

loadCustomerAssets() {
    getCustomerAssets()
    .then(result => {
        this.customerAssets = result;
    })
    .catch(error => {
        this.error = error;
    })
}

handleAssetClick(evt){
    const assetSelectedEvent = new CustomEvent("assetselected", {
        detail : evt.detail
    });

    this.dispatchEvent(assetSelectedEvent);
}

}